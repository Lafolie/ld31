local function genColor()
	local c = {}
	for i = 1, 3 do
		c[i] = (love.math.random(0, 255) + 255)/2
	end
	return c
end

local randColor
do
	local function alike(c1, c2)
		local dist = 255
		for i = 1, 3 do
			local x = c1[i]-c2[i]
			dist = math.min(dist, x*x)
		end
		return dist < 25
	end

	local existingColors = {}
	function randColor()
		local rejected = true
		local c
		while rejected do
			c, rejected = genColor(), false
			for i, v in ipairs(existingColors) do
				if alike(v, c) then
					rejected = true
					break
				end
			end
		end

		table.insert(existingColors, c)
		return c
	end
end

class "Player"
{
	scrap = 1000, -- FIXME
	alive = true,

	__init__ = function(self)
		self.units = {}
		self.color = randColor()
		self.bases = {}
	end,

	addUnit = function(self, unit)
		table.insert(self.units, unit)
	end,

	iterUnits = function(self)
		return ipairs(self.units)
	end,

	getUnits = function(self)
		return self.units
	end,

	getUnitCount = function(self)
		return #self.units
	end,

	getColor = function(self)
		return self.color
	end,

	removeUnit= function(self, unit)
		for i, v in self:iterUnits() do
			if v == unit then
				table.remove(self.units, i)
				return
			end
		end
	end,

	killUnit = function(self, unit)
		unit:die(self)
		return self:removeUnit(unit) -- SHH, don't tell anyone this is equal
	end,

	startTurn = function(self, game)
	end,

	update = function(self, game, dt)
	end,

	userInputMovement = function(self, game, x, y)
	end,

	userPrimaryAction = function(self, game, x, y)
	end,

	userSecondaryAction = function(self, game, x, y)
	end,

	hasBaseAt = function(self, x, y)
		for i, v in ipairs(self.bases) do
			if v.x == x and v.y == y then
				return true
			end
		end
		return false
	end,

	ownsUnit = function(self, unit)
		for i, v in self:iterUnits() do
			if v == unit then
				return true
			end
		end
		return false
	end,
}
