require "class.Unit"
local cache = require "lib.cache"

class "Soldier" (Unit)
{
	name = "Solider",
	texture = cache.image("gfx/soldier.png")
}
