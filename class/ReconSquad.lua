require "class.Unit"
local cache =  require "lib.cache"

return class "ReconSquad" (Unit)
{
	name = "Recon Squad",
	type = "infantry",
	moveRange = 4,
	cost = 250,
	weaponSound = soundfx.laser,

	weapons = {
		{
			name = "LRG",
			range = 4,
			shotsPerAttack = 1,
			weaponAccuracy = 0, -- DAMMIT DALE
			weaponDamage =
			{
				infantry = 17,
				vehicle = 2,
				tank = 0,
			},
		},
		{
			name = "Rawk",
			range = 1,
			shotsPerAttack = 5,
			weaponAccuracy = 0.7,
			weaponDamage =
			{
				infantry = 1,
				vehicle = 0.5,
				tank = 0,
			},
		},
	},

	__init__ = function(self, ...)
		Unit.__init__(self, ...)
		self:loadInfantryAnimation(cache.image("gfx/units/recon.png"))
	end,

	attackWithWeapon = function(self, other, weapon)
		self.weapons[1].weaponAccuracy = self:hasMoved() and 0.40 or 0.95
		return Unit.attackWithWeapon(self, other, weapon)
	end,

	attack = function(self, other)
		local dmg = math.floor(self:attackWithWeapon(other, 1))
		print(("Dealt %d points of damage"):format(dmg))
		return dmg
	end,

	counterAttack = function(self, other)
		local dmg = math.floor(self:attackWithWeapon(other, 2))
		print(("Dealt %d points of damage"):format(dmg))
		return dmg
	end,
}
