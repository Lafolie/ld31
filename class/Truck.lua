require "class.Unit"
local cache = require "lib.cache"

return class "Truck" (Unit)
{
	name = "Truck",
	type = "vehicle",
	moveRange = 6,
	texture = cache.image("gfx/truck.png"),
	cost = 200,

	weapons = {
	},

	cargo = {
		scrap = 500,
		infantry = 2,
	},

	__init__ = function(self, x, y, map)
		Unit.__init__(self, x, y, map)
		self.carrying = {
			scrap = 0,
			infantry = 0,
		}

		self.embarkedUnits = {}
	end,

	canEmbark = function(self, unit)
		if self.carrying.scrap > 0 then
			return false
		end

		if unit.type ~= "infantry" then
			return false
		end

		if self.carrying.infantry >= self.cargo.infantry then
			return false
		end

		return true
	end,

	embark = function(self, unit)
		if not self:canEmbark(unit) then return false end

		self.carrying.infantry = self.carrying.infantry + 1
		table.insert(self.embarkedUnits, unit)
		return true
	end,

	disembark = function(self)
		if self.carrying.infantry == 0 then
			return nil
		end

		self.carrying.infantry = self.carrying.infantry - 1
		return table.remove(self.embarkedUnits, #self.embarkedUnits)
	end,
}
