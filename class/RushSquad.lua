require "class.Unit"
local cache =  require "lib.cache"

return class "RushSquad" (Unit)
{
	name = "Rush Squad",
	type = "infantry",
	moveRange = 4,
	cost = 100,

	weapons = {
		{
			name = "AR26",
			range = 1,
			shotsPerAttack = 5,
			weaponAccuracy = 0.80,
			weaponDamage =
			{
				infantry = 2,
				vehicle = 0.75,
				tank = 0.1,
			},
		},
	},

	__init__ = function(self, ...)
		Unit.__init__(self, ...)
		self:loadInfantryAnimation(cache.image("gfx/units/rush.png"))
	end,
}
