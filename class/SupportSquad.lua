require "class.Unit"
local cache =  require "lib.cache"

return class "SupportSquad" (Unit)
{
	name = "Support Squad",
	type = "infantry",
	moveRange = 3,
	cost = 250,

	weapons = {
		{
			name = "M9000",
			range = 1,
			shotsPerAttack = 20,
			weaponAccuracy =
			{
				infantry = 0, -- dammit dale
				vehicle = 0.80,
				tank = 0.80,
			},
			weaponDamage =
			{
				infantry = 0.6,
				vehicle = 0.4,
				tank = 0.1,
			},
		},
	},

	__init__ = function(self, ...)
		Unit.__init__(self, ...)
		self:loadInfantryAnimation(cache.image("gfx/units/support.png"))
	end,

	attackWithWeapon = function(self, other, weapon)
		self.weapons[1].weaponAccuracy.infantry = 60*(100 - other.hp)+20
		return Unit.attackWithWeapon(self, other, weapon)
	end,
	
	canHeal = function(self,other)
		return other.type == "infantry" and other.hp < 100
	end,
	
	heal = function(self,other)
		return self:ability(other)
	end,

	ability = function(self, other)
		if other.type ~= "infantry" then
			return false
		end

		other.hp = math.min(100, other.hp + 6*math.floor(other.hp/20))
		return true
	end,
}
