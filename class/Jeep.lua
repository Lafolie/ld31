require "class.Unit"
require "class.RushSquad"
local cache = require "lib.cache"

return class "Jeep" (Unit)
{
	name = "Jeep",
	type = "vehicle",
	moveRange = 6,
	cost = 400,
	texture = cache.image("gfx/jeep.png"),

	weapons = {
		{
			name = "DRVB1 Mounted MG",
			range = 1,
			shotsPerAttack = 50,
			weaponAccuracy = 0.60,
			weaponDamage =
			{
				infantry = 1.5,
				vehicle = 1,
				tank = 0.5,
			},
		},
	},
	die = function(self,player)
		local driver = RushSquad(self.x,self.y, self.map)
		driver.hp = 60
		displayHp = 60
		player:addUnit(driver)
		Unit.die(self,player)
	end
}
