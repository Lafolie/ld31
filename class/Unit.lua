vector = require "lib.hump.vector"
local cache =  require "lib.cache"
local newAnimation = require "lib.AnAL"

local tinyFont = cache.font("font/wendy.ttf:20")
local damageFont = cache.font("font/wendy.ttf:16")

local drawOffsets =
{
	{8, 8},
	{0, 0},
	{16, 0},
	{16, 16},
	{0, 16},
}

return class "Unit"
{
	name = "Your coder is lazy and forgot to name this",
	x = 0,
	y = 0,
	currentPosition = vector(1,1),
	type = "infantry",
	moveRange = 5,
	hp = 100,
	displayHp = 100,
	moved = false,
	pathNodes = {},
	movementSpeed = 8,
	texture = cache.image("gfx/placeholder.png"),
	animation = nil,
	actionSelected = false,
	weaponSound = soundfx.fire,

	weapons =
	{
		{
			name = "Shooty gun",
			range = 1,
			shotsPerAttack = 0,
			hp = 100,
			weaponAccuracy = 0,
			weaponDamage = 1,
			weaponRange = 1,
		},
	},

	cargo =
	{
		scrap = 0,
		infantry = 0,
	},

	carrying =
	{
		scrap = 0,
		infantry = 0,
	},

	__init__ = function(self, x, y,map,player)
		self.x, self.y = x, y
		self.currentPosition = vector(x, y)
		self.map = map
		self.displayHp = self.hp
		if self.type == "infantry" then -- FIXME
			local animSheet = cache.image("gfx/soldier.png")
			self:loadInfantryAnimation(animSheet)
		end
	end,

	loadInfantryAnimation = function(self, animSheet)
		self.animation =
		{
			idle = newAnimation(animSheet, 17, 17, 0.3, 0),
			moving = newAnimation(animSheet, 17, 17, 0.1, 0),
		}
		local frames = {}
		for i = 1, 2 do
			table.insert(frames, self.animation.idle.frames[i])
		end
		self.animation.idle.frames, frames = frames, {}
		for i = 6, 10 do
			table.insert(frames, self.animation.moving.frames[i])
		end
		self.animation.moving.frames, frames = frames, {}
	end,

	move = function(self, x, y)
		if x == self.x and y == self.y then -- allow zero length movement
			self.previousPosition = vector(self.x,self.y)
		end
		
		if not self.previousPosition then
			local path = self.map:getPath(self.x,self.y,x,y, false, self.type ~= "infantry")
			if path then
				local length = path:getLength()
				if length <= self.moveRange then
					self.previousPosition = vector(self.x,self.y)
					self.pathNodes = {}
					for node, count in path:nodes() do
						self.pathNodes[count] = vector(node:getX(),node:getY())
					end
					table.remove(self.pathNodes,1)
				end
			end
		end
	end,
	
	startTurn = function(self)
		self.previousPosition = nil
		self.actionSelected = false
		self.turnComlete = false
	end,

	hasMoved = function(self)
		return self.previousPosition
	end,
	
	undoMovement = function(self)
		if self.previousPosition then
			self.x, self.y = self.previousPosition.x, self.previousPosition.y
			self.currentPosition  = self.previousPosition
			self.previousPosition = nil
			self.pathNodes = {}
			self.actionSelected = false
			return true
		else
			return false
		end
	end,

	attack = function(self, other)
		self.weaponSound:stop()
		self.weaponSound:play()
		local dmg = 0
		for i = 1, #self.weapons do
			dmg = dmg + self:attackWithWeapon(other, i)
		end

		dmg = math.floor(dmg)
		print(("Dealt %d points of damage"):format(dmg))
		return dmg
	end,

	counterAttack = function(self, other)
		return self:attack(other)
	end,

	attackWithWeapon = function(self, other, weapon)
		if not self.weapons[weapon] then return 0 end

		local weap = self.weapons[weapon]

		local dmg = 0
		local shots = weap.shotsPerAttack
		if self.type == "infantry" then
			shots = shots*math.ceil(self.hp/20)
		end

		local accuracy = weap.weaponAccuracy
		if not tonumber(accuracy) then
			accuracy = weap.weaponAccuracy[other.type]
		end

		local accuracyMultiplier = self.map:getAccuracyMultiplier(other.x, other.y)
		accuracy = accuracy * accuracyMultiplier

		local damage = weap.weaponDamage
		if not tonumber(damage) then
			damage = weap.weaponDamage[other.type]
		end

		print("Firing ", shots, " shots, for ", damage, " damage each, with an accuracy of ", accuracy)
		for shot = 1, shots do
			dmg = love.math.random() < accuracy and dmg + damage or dmg
		end

		return dmg
	end,

	ability = function(self, other)
		return false
	end,

	canEmbark = function(self, unit)
		return false
	end,
	
	canHeal = function(self, unit)
		return false
	end,
	
	canRepair = function(self, unit)
		return false
	end,

	embark = function(self, unit)
		return false
	end,
	
	update = function(self,dt)
		if self.displayHp ~= self.hp then
			if not self.damagePopup then
				self.damagePopup = self.hp - self.displayHp
			end
			local diff = self.hp - self.displayHp
			if diff < -10 then
				diff = -10
			elseif diff > 10 then
				diff = 10
			end
			if diff < 0 then
				self.displayHp = math.floor(self.displayHp + diff*dt)
			else
				self.displayHp = math.ceil(self.displayHp + diff*dt)
			end
			if math.abs(self.displayHp - self.hp) < 2 then
				self.displayHp = self.hp
			end

			if self.displayHp == self.hp then
				self.damagePopup = nil
			end
		end

		if self.animation then
			self.animation.idle:update(dt)
			self.animation.moving:update(dt)
		end

		if #self.pathNodes > 0 then
			local nextNode = self.pathNodes[1]
			local direction = nextNode - self.currentPosition
			local directionNormalised = direction:normalized()
			direction = directionNormalised *  self.movementSpeed * dt
			self.currentPosition = self.currentPosition + direction
			local newDirection = nextNode - self.currentPosition
			newDirection:normalize_inplace()
			local diffx = directionNormalised.x - newDirection.x
			local diffy = directionNormalised.y - newDirection.y 			
			if diffx > 0.1 or diffx < -0.1 or diffy > 0.1 or diffy < -0.1  then
				self.currentPosition = self.pathNodes[1]
				table.remove(self.pathNodes,1)
			end
			self.x, self.y = math.floor(self.currentPosition.x+0.5), math.floor(self.currentPosition.y+0.5)
			
		end		
	end,

	isMoving = function(self)
		return #self.pathNodes > 0
	end,

	draw = function(self, flip)
		local x = (self.currentPosition.x-1)*gridSize
		local y = (self.currentPosition.y-1)*gridSize
		if self.type == "infantry" then
			for i = 1, self:getNumAlive() do
				if self.animation then
					local anim = self:isMoving() and self.animation.moving or self.animation.idle
					anim:draw(drawOffsets[i][1]+x, drawOffsets[i][2]+y, 0, flip and -1 or 1, 1, flip and 16 or 0, 0)
				else
					love.graphics.draw(self.texture, drawOffsets[i][1]+x, drawOffsets[i][2]+y, 0, flip and -1 or 1, 1, flip and 16 or 0, 0)
				end
			end
		else
			love.graphics.draw(self.texture, x, y, 0, flip and -1 or 1, 1, flip and 32 or 0, 0)
		end
		if self.hp < 100 then
			love.graphics.setFont(tinyFont)
			love.graphics.setColor(0, 0, 0)
			love.graphics.print(self.displayHp, (self.currentPosition.x-1)*gridSize+18, (self.currentPosition.y-1)*gridSize+17)
			love.graphics.setColor(255, 255, 255)
			love.graphics.print(self.displayHp, (self.currentPosition.x-1)*gridSize+17, (self.currentPosition.y-1)*gridSize+16)
		end
		if self.damagePopup then
			love.graphics.setFont(damageFont)
			love.graphics.setColor(0, 0, 0)
			love.graphics.print(self.damagePopup, (self.currentPosition.x-1)*gridSize+11, (self.currentPosition.y-1)*gridSize-18)
			if self.damagePopup < 0 then
				love.graphics.setColor(255, 0, 0)
			else
				love.graphics.setColor(0, 255, 0)
			end
			love.graphics.print(self.damagePopup, (self.currentPosition.x-1)*gridSize+10, (self.currentPosition.y-1)*gridSize-17)
		end
		if self.turnComlete then
			love.graphics.setFont(damageFont)
			love.graphics.setColor(0, 0, 0)
			love.graphics.print("X", (self.currentPosition.x-1)*gridSize, (self.currentPosition.y-1)*gridSize+11)
			love.graphics.setColor(255,0, 0)
			love.graphics.print("X", (self.currentPosition.x-1)*gridSize, (self.currentPosition.y-1)*gridSize+16)
		end
		
		local r, g, b = love.graphics.getColor()
		love.graphics.setColor(255, 255, 255)
		love.graphics.setFont(tinyFont)
		if self.carrying and self.carrying.scrap > 0 then
			love.graphics.print(self.carrying.scrap, (self.currentPosition.x-1)*gridSize, (self.currentPosition.y-1)*gridSize)
		elseif self.carrying and self.carrying.infantry > 0 then
			love.graphics.print(self.carrying.infantry, (self.currentPosition.x-1)*gridSize, (self.currentPosition.y-1)*gridSize)
		end
		love.graphics.setColor(r, g, b)
	end,

	getNumAlive = function(self)
		if self.type ~= "infantry" then
			return math.ceil(self.hp/100)
		end
		return math.ceil(self.hp/20)
	end,

	canSalvage = function(self)
		return self.cargo.scrap > self.carrying.scrap and self.carrying.infantry == 0
	end,

	salvage = function(self, scrap)
		local remaining = self.cargo.scrap - self.carrying.scrap
		scrap = math.min(scrap, remaining)
		self.carrying.scrap = self.carrying.scrap + scrap
		return scrap
	end,
	
	die = function(self,player)
		soundfx.death:stop()
		soundfx.death:play()
		self.map:dropScrap(self.x,self.y,self.cost * 0.75)
	end,
}
