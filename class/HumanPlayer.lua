require "class.Player"

return class "HumanPlayer" (Player)
{
	userInputMovement = function(self, game, x, y)
		return game:moveSelectionCursor(self, x, y)
	end,

	userPrimaryAction = function(self, game, x, y)
		return game:performPrimaryActionOnTile(self, x, y)
	end,

	userSecondaryAction = function(self, game, x, y)
		return game:performSecondaryActionOnTile(self, x, y)
	end,
}
