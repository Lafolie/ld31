require "class.Unit"
local cache = require "lib.cache"

return class "Tank" (Unit)
{
	name = "Tank",
	type = "tank",
	moveRange = 5,
	texture = cache.image("gfx/tank.png"),
	cost = 800,

	weapons = {
		{
			name = "120mm Cannon",
			range = 1,
			shotsPerAttack = 1,
			weaponAccuracy =
			{
				infantry = 0.40,
				vehicle = 0.75,
				tank = 0.75,
			},
			weaponDamage =
			{
				infantry = 50,
				vehicle = 60,
				tank = 40,
			},
		},
		{
			name = "LMG Turret",
			shotsPerAttack = 20,
			weaponAccuracy = 0.60,
			weaponDamage =
			{
				infantry = 2,
				vehicle = 1,
				tank = 0.1,
			},
		},
	},
}
