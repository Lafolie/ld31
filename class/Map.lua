local cache = require "lib.cache"
local Grid = require ("lib.jumper.grid") -- The grid class
local Pathfinder = require ("lib.jumper.pathfinder") -- The pathfinder
local newAnimation = require "lib.AnAL"

local wallimg = cache.image("gfx/shittywall.png")

local function loadImagesetTileset(outtiles, tiles, firstgid)
	for j, w in ipairs(tiles) do
		outtiles[w.id+firstgid] =
		{
			img = cache.image(w.image),
			quad = love.graphics.newQuad(0, 0, gridSize, gridSize, gridSize, gridSize)
		}
	end
end

local function loadImagebasedTileset(outtiles, image, firstgid)
	local image = cache.image(image)
	local id = firstgid
	local w, h = image:getDimensions()
	local padding = 1
	for y = 0, h-1, gridSize+padding do
		for x = 0, w-1, gridSize+padding do
			outtiles[id] =
			{
				img = image,
				quad = love.graphics.newQuad(x, y, gridSize+padding, gridSize+padding, w, h)
			}
			id = id + 1
		end
	end
end

local function copyProperties(p)
	local new = {}
	for i, v in pairs(p) do
		if v == "true" then
			new[i] = true
		elseif v == "false" then
			new[i] = false
		elseif tonumber(v) then
			new[i] = tonumber(v)
		else
			new[i] = v
		end
	end

	return new
end

class "Map"
{
	__init__ = function(self, mapdata, unitIter)
		self.mapdata = mapdata
		self.unitIter = unitIter

		self.tilesets = {}
		self.tiles = {}
		self.layers = {}
		self.smokingtiles = {}
		self.ownedBases = {}

		local smokeImg = cache.image("gfx/smokeAnim.png")
		self.smoke = newAnimation(smokeImg, 33, 66, 0.15, 0)
		self.width, self.height = mapdata.width, mapdata.height

		for i, v in ipairs(mapdata.tilesets) do
			if v.image then
				loadImagebasedTileset(self.tiles, v.image, v.firstgid)
			else
				loadImagesetTileset(self.tiles, v.tiles, v.firstgid)
			end

			for j, w in ipairs(v.tiles) do
				self.tiles[v.firstgid+w.id].properties = copyProperties(w.properties or {})
			end

			for j = v.firstgid, #self.tiles do
				local w = self.tiles[j]
				w.properties = w.properties or {}

				local solid = w.properties and w.properties.solid
				w.walkable = not solid
				w.driveable = not solid and not w.properties.undriveable
			end
		end

		for i, v in ipairs(mapdata.layers) do
			local layer = {}
			local i = 1
			for y = 1, mapdata.height do
				layer[y] = {}
				for x = 1, mapdata.width do
					layer[y][x] = v.data[i]
					if v.data[i] ~= 0 and self.tiles[v.data[i]].properties.smoking then
						table.insert(self.smokingtiles, {x, y})
					end
					if v.data[i] ~= 0 and self.tiles[v.data[i]].properties.ownedBy then
						table.insert(self.ownedBases, {x, y, self.tiles[v.data[i]].properties.ownedBy})
					end
					i = i + 1
				end
			end
			table.insert(self.layers, layer)
		end	

		self:generateCollisionGrid()
		self:createScrapTable()
	end,

	inRange = function(self, x, y)
		return x > 0 and y > 0 and x <= self.width and y <= self.height
	end,

	getTile = function(self, x, y)
		for i = #self.layers, 1, -1 do
			if self.layers[i][y][x] ~= 0  and i~= 6 then
				return self.tiles[self.layers[i][y][x]]
			end
		end
	end,

	getScrap = function(self, x, y)
		return self.scrapTable[y][x]
	end,
	
	setScrap = function(self,amount,x,y)
		self.scrapTable[y][x] = amount
		if amount >= 2500 then
			self.layers[6][y][x] = 78
		elseif amount > 500 then
			self.layers[6][y][x] = 77
		elseif amount > 0 then
			self.layers[6][y][x] = 76
		else
			self.layers[6][y][x] = 0
		end
	end,
	
	createScrapTable = function(self)
		self.scrapTable = {}
		for y = 1, self.height do
			self.scrapTable[y]={}
			for x = 1, self.width do
				local tile = self.layers[6][y][x]
				if tile == 76 then
					self.scrapTable[y][x] = 500
				elseif tile == 77 then
					self.scrapTable[y][x] = 2000
				elseif tile ==  78 then
					self.scrapTable[y][x] = 5000
				else
					self.scrapTable[y][x] = 0
				end
			end
		end
	end,
	
	getScrapLocations = function(self)
		if not self.ScrapLocations then
			self.ScrapLocations = {}
			for y = 1, self.height do
				for x = 1, self.width do
					if self:getScrap(x,y) > 0 then
						table.insert(self.ScrapLocations,{x=x,y=y})
					end
				end
			end
		end
		return self.ScrapLocations
	end,
	
	getBaseLocations = function(self)
		if not self.BaseLocations then
			self.BaseLocations = {}
			for y = 1, self.height do
				for x = 1, self.width do
					local tile = self:getTile(x,y)
					if tile.properties.base then
						table.insert(self.BaseLocations,{x=x,y=y})
					end
				end
			end
		end
		return self.BaseLocations
	end,

	salvage = function(self, x, y, amount)
		self.ScrapLocations = nil
		local scrap = self:getScrap(x,y)
		self:setScrap(scrap-amount,x,y)
	end,
	
	dropScrap = function(self,x,y,amount)
		self.ScrapLocations = nil
		local tile = self.layers[5][y][x]
		local scrap = self:getScrap(x,y)
		self:setScrap(scrap+amount,x,y)
	end,

	getAccuracyMultiplier = function(self, x, y)
		local tile = self:getTile(x, y)
		return tile.properties.accuracyMultiplier or 1
	end,

	draw = function(self, players)
		self.smoke:update(love.timer.getDelta())
		love.graphics.setColor(255, 255, 255)
		for i, v in ipairs(self.layers) do
			for y = 1, self.height do
				for x = 1, self.width do
					if v[y][x] ~= 0 then
						local tile = self.tiles[v[y][x]]
						love.graphics.draw(tile.img, tile.quad, (x-1)*gridSize, (y-1)*gridSize)
					end
				end
			end
		end

		for i, v in ipairs(players) do
			love.graphics.setColor(v:getColor())
			for j, w in ipairs(v.bases) do
				if w ~= v.homeBase then
					love.graphics.draw(wallimg, (w.x-1)*gridSize, (w.y-1)*gridSize)
				end
			end
		end

		love.graphics.setColor(255, 255, 255)
		for i, v in ipairs(self.smokingtiles) do
			self.smoke:draw((v[1]-1)*gridSize, (v[2]-2)*gridSize)
		end
	end,
	
	getCollisionGrid = function(self, ignoreUnits, vehicleMovement,ignorePoint)
		local newGrid = {}
		for y = 1, self.height do
			newGrid[y] = {}
			for x = 1, self.width do
				if vehicleMovement then
					newGrid[y][x] = self.gridTable[y][x]
				else
					newGrid[y][x] = self.gridTable[y][x]%2
				end
			end
		end

		if not ignoreUnits then
			for unit in self.unitIter() do
				newGrid[unit.y][unit.x] = 1
			end
		end
		
		if ignorePoint then
			newGrid[ignorePoint.y][ignorePoint.x] = 0
		end
		

		return Grid(newGrid)
	end,

	generateCollisionGrid = function(self)
		local gridTable = {}
		for i, v in ipairs(self.layers) do
			for y = 1, self.height do
				gridTable[y] = gridTable[y] or {}
				for x = 1, self.width do
					if v[y][x] ~= 0 then
						local tile = self.tiles[v[y][x]]
						if gridTable[y][x] then
							gridTable[y][x] = math.max(tile.driveable and 0 or 2,gridTable[y][x] )
						else
							gridTable[y][x] = tile.driveable and 0 or 2
						end
					
						if not tile.walkable then
							gridTable[y][x] = 1
						end
					end
				end
			end
		end
		self.gridTable = gridTable
	end,
	
	isPassable = function(self,x,y,vehicleMovement)
		if not ignoreUnits then
			for unit in self.unitIter() do
				if unit.y == y and unit.x == x then
					return 1
				end
			end
		end
	
	
		if vehicleMovement then
			return  self.gridTable[y][x] == 0
		else
			return self.gridTable[y][x]%2 == 0
		end
	end,
	
	getPath = function(self,startx,starty,endx,endy, ignoreUnits, vehicleMovement,ignorePoint)
		local finder = Pathfinder(self:getCollisionGrid(ignoreUnits, vehicleMovement,ignorePoint),'ASTAR',0)
		finder:setMode('ORTHOGONAL')
		return finder:getPath(startx,starty,endx,endy)
	end,
}
