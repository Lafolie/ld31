require "class.Unit"
local cache =  require "lib.cache"

return class "TechSquad" (Unit)
{
	name = "Tech Squad",
	type = "infantry",
	moveRange = 3,
	cost = 400,

	weapons = {
		{
			name = "GRL",
			shotsPerAttack = 1,
			range = 1,
			weaponAccuracy =
			{
				infantry = 0.40,
				vehicle = 0.90,
				tank = 0.90,
			},
			weaponDamage =
			{
				infantry = 14,
				vehicle = 18,
				tank = 16,
			},
		},
	},

	__init__ = function(self, ...)
		Unit.__init__(self, ...)
		self:loadInfantryAnimation(cache.image("gfx/units/tech.png"))
	end,
	
	
	canRepair = function(self,other)
		return other.type ~= "infantry" and other.hp < 100
	end,
	
	repair = function(self,other)
		return self:ability(other)
	end,

	ability = function(self, other)
		if other.type == "infantry" then
			return false
		end

		other.hp = math.min(100, other.hp + 6*math.floor(other.hp/20))
		return true
	end,
}
