require "class.Player"
vector = require "lib.hump.vector"
local Gamestate = require "lib.hump.gamestate"

local unitSelection =
{
	require "class.Jeep",
	require "class.ReconSquad",
	require "class.RushSquad",
	require "class.SupportSquad",
	require "class.Tank",
	require "class.TechSquad",
	require "class.Truck",
}

class "AiPlayer" (Player)
{
	cursorSpeed = 4,
	selectCursorSpeed = 8,
	moveCursorSpeed = 4,
	menuWait = 0,
	lastAction = "",

	update = function(self,game,dt)
		-- end turn if no more units
		if #self.unitsToMove < 1  and #self.basesToUpdate < 1 then
			game:nextPlayersTurn()
			return
		end
		
		local actionMenu = Gamestate.current()
		if actionMenu.addOptions then
			-- do action menu things
			self.menuWait = self.menuWait + dt
			if self.menuWait < 0.5 then
				return
			else
				self.menuWait = 0
			end
			
			if not actionMenu.selection then
				if self:isActionAvailible(actionMenu,"Heal") and love.math.random(1,100) < 50 then
					self:selectMenuItem(actionMenu,"Heal")
					return
				end
				if self:isActionAvailible(actionMenu,"Repair") and love.math.random(1,100) < 50 then
					self:selectMenuItem(actionMenu,"Repair")
					return
				end
				if self:isActionAvailible(actionMenu,"Attack")then
					self:selectMenuItem(actionMenu,"Attack")
					return
				end
				if self:isActionAvailible(actionMenu,"Heal")then
					self:selectMenuItem(actionMenu,"Heal")
					return
				end
				if self:isActionAvailible(actionMenu,"Repair")then
					self:selectMenuItem(actionMenu,"Repair")
					return
				end
				if self:isActionAvailible(actionMenu,"Salvage")then
					self:selectMenuItem(actionMenu,"Salvage")
					return
				end
				if self:isActionAvailible(actionMenu,"Capture")then
					self:selectMenuItem(actionMenu,"Capture")
					return
				end
				if self:isActionAvailible(actionMenu,"Unload")then
					self:selectMenuItem(actionMenu,"Unload")
					return
				end
				if self:isActionAvailible(actionMenu,"Embark")then
					self:selectMenuItem(actionMenu,"Embark")
					return
				end
				if self:isActionAvailible(actionMenu,"Disembark")then
					self:selectMenuItem(actionMenu,"Disembark")
					return
				end
				self:selectMenuItem(actionMenu,"Wait")
			else
				--select target
				if self.lastAction == "Attack" then
					if self.currentUnit.type == "vehicle" then
						self:selectTargetVehicle(self.currentUnit,actionMenu)
					elseif self.currentUnit.type == "infantry" then
						self:selectTargetInfantry(self.currentUnit,actionMenu)
					elseif self.currentUnit.type == "tank" then
						self:selectTargetTank(self.currentUnit,actionMenu)
					end
				else
					actionMenu:keypressed("return",self)
				end
			end
			return
		end
		
		local buyMenu = Gamestate.current()
		if buyMenu.hack then
			--do buyMenu things
			
			self.menuWait = self.menuWait + dt
			if self.menuWait < 0.5 then
				return
			else
				self.menuWait = 0
			end
			self:selectBuyMenuItem(buyMenu,self.currentBase.unit)
			return
		end
		
		-- set next base
		if not self.currentBase and #self.basesToUpdate > 0 then
			self.currentBase = self.basesToUpdate[1]
			return
		end
		
		-- do base stuff
		if self.currentBase then
			if not self.buyMenuOpen then
				self:openBuyMenu(self.currentBase.base,game,dt)
			end
			return
		end
		
		
		
		
		--set current unit
		if not self.currentUnit then
			self.currentUnit = self.unitsToMove[1]
		end

		--select current unit
		if self.currentUnit ~= game.selectedUnit then
			--game.selectedUnit = nil
			self:selectUnit(self.currentUnit,game,dt)
			return
		end
		
		-- set next move
		if not self.moveLocation then
			if self.currentUnit.type == "vehicle" then
				self:setMoveLocationVehicle(self.currentUnit,game)
			elseif self.currentUnit.type == "infantry" then
				self:setMoveLocationInfantry(self.currentUnit,game)
			elseif self.currentUnit.type == "tank" then
				self:setMoveLocationTank(self.currentUnit,game)
			end
			return
		end

		-- send move order
		if not self.currentUnit.previousPosition then
			self:moveToMoveLocation(unit,game,dt)
			return
		end

		--wait for move completion
		if self.currentUnit.x ~= self.moveLocation.x or self.currentUnit.y ~= self.moveLocation.y then
			return
		end

		-- wait for attack menu
		if not self.currentUnit.actionSelected then
			return
		end
		

		-- unit actions complete
		table.remove(self.unitsToMove,1)
		self.currentUnit = nil
		self.moveLocation = nil
		game.selectedUnit = nil
		
	end,
	
	
	openBuyMenu = function(self,base,game,dt)
		if game.selection.x == base.x and game.selection.y == base.y then
			game:performPrimaryActionOnTile(self,game.selection.x,game.selection.y)
			self.buyMenuOpen = true
		else
			self.cursorSpeed = self.selectCursorSpeed
			self:moveCursor(game,base.x,base.y,dt)
		end
	end,
	
	selectBuyMenuItem = function(self,buyMenu,unit)
		if unitSelection[buyMenu.selected] == unit then
			buyMenu:keypressed("return",self)
			self.buyMenuOpen = false
			self.currentBase = nil 
			table.remove(self.basesToUpdate,1)
			table.insert(self.unitsToMove,1,self.units[#self.units])
		else
			buyMenu:keypressed("down",self)
		end
	end,
	
	setMoveLocationVehicle = function(self,vehicle,game)
		if vehicle.__name__ == "Truck" then
			self:setMoveLocationTruck(vehicle,game)
		else
			if not self:trySetWithinRangeNearestEnemy(vehicle,game,"infantry") then
				if not self:trySetWithinRangeNearestEnemy(vehicle,game,"vehicle") then
					if not self:trySetWithinRangeNearestEnemy(vehicle,game,"tank") then
						self:setRandomPosition(vehicle,game)
					end
				end
			end
		end
	end,
	
	setMoveLocationTruck = function(self,truck,game)
		if truck.carrying.scrap < truck.cargo.scrap then
			self:setNearestToScrap(truck,game)
		else
			self:setNearestToFriendlyBase(truck,game)
		end
	end,
	
	selectTargetVehicle = function(self,vehicle,actionMenu)
		self:selectTargetType(actionMenu,"infantry","vehicle","tank")
	end,
	
	setMoveLocationInfantry = function(self,infantry,game)
		if (#self.bases < 2 or love.math.random(1,100) < 20) and self:trySetNearestToCapturableBase(infantry,game)then 
		elseif self:moveIfWithinRangeCapturableBase(infantry,game) then
		elseif infantry.__name__ ==	"TechSquad" then
			self:setMoveLocationTechSquad(infantry,game)
		elseif infantry.__name__ ==	"SupportSquad" then
			self:setMoveLocationSupportSquad(infantry,game)
		elseif infantry.__name__ ==	"RushSquad" then
			self:setMoveLocationRushSquad(infantry,game)
		elseif infantry.__name__ ==	"ReconSquad" then
			self:setMoveLocationReconSquad(infantry,game)			
		else
			self:setNearestToEnemy(infantry,game)
		end
	end,
	
	selectTargetInfantry = function(self,infantry,actionMenu)
		if infantry.__name__ ==	"TechSquad" then
			self:selectTargetTechSquad(actionMenu)
		elseif infantry.__name__ ==	"SupportSquad" then
			self:selectTargetSupportSquad(actionMenu)
		elseif infantry.__name__ ==	"RushSquad" then
			self:selectTargetRushSquad(actionMenu)
		elseif infantry.__name__ ==	"ReconSquad" then
			self:selectTargetReconSquad(actionMenu)			
		end
	end,
	
	
	
	setMoveLocationTechSquad = function(self,techSquad,game)
		if self:trySetWithinRangeNearestEnemy(techSquad,game,"tank") then
		elseif self:trySetWithinRangeNearestEnemy(techSquad,game,"vehicle") then
		elseif self:trySetWithinRangeNearestEnemy(techSquad,game,"infantry")then
		else
			self:setRandomPosition(techSquad,game)
		end
	end,
	
	selectTargetTechSquad = function(self,actionMenu)
		self:selectTargetType(actionMenu,"tank","vehicle","infantry")
	end,
	
	setMoveLocationSupportSquad = function(self,supportSquad,game)
		if self:trySetWithinRangeNearestEnemy(supportSquad,game,"vehicle") then
		elseif self:trySetWithinRangeNearestEnemy(supportSquad,game,"infantry") then
		elseif self:trySetWithinRangeNearestEnemy(supportSquad,game,"tank")then
		else
			self:setRandomPosition(supportSquad,game)
		end
	end,
	
	selectTargetSupportSquad = function(self,actionMenu)
		self:selectTargetType(actionMenu,"vehicle","infantry","tank")
	end,
	
	setMoveLocationRushSquad = function(self,rushSquad,game)
		if self:trySetWithinRangeNearestEnemy(rushSquad,game,"vehicle") then
		elseif self:trySetWithinRangeNearestEnemy(rushSquad,game,"infantry") then
		elseif self:trySetWithinRangeNearestEnemy(rushSquad,game,"tank")then
		else
			self:setRandomPosition(rushSquad,game)
		end
	end,
	
	selectTargetRushSquad  = function(self,actionMenu)
		self:selectTargetType(actionMenu,"vehicle","infantry","tank")
	end,
	
	setMoveLocationReconSquad = function(self,reconSquad,game)
		if self:trySetWithinRangeNearestEnemy(reconSquad,game,"infantry") then
		elseif self:trySetWithinRangeNearestEnemy(reconSquad,game,"vehicle") then
		elseif self:trySetNearestToCapturableBase(reconSquad,game) then
		else
			self:setRandomPosition(reconSquad,game)
		end
	end,
	
	selectTargetReconSquad  = function(self,actionMenu)
		self:selectTargetType(actionMenu,"infantry","vehicle","tank")
	end,
	
	
	setMoveLocationTank = function(self,tank,game)
		if not self:trySetWithinRangeNearestEnemy(tank,game,"vehicle") then
			if not self:trySetWithinRangeNearestEnemy(tank,game,"infantry") then
				if not self:trySetWithinRangeNearestEnemy(tank,game,"tank") then
					self:setRandomPosition(tank,game)
				end
			end
		end
	end,
	
	selectTargetTank = function(self,tank,actionMenu)
		self:selectTargetType(actionMenu,"tank","vehicle","infantry")
	end,	
	
	selectMenuItem = function(self,actionMenu,item)
		if actionMenu.options[actionMenu.currentOption] == item then
			actionMenu:keypressed("return",self)
			self.lastAction = item
		else
			actionMenu:keypressed("down",self)
		end
	end,
	
	selectTargetType = function(self,actionMenu,primary,secondary,tertiary)
		local secondaryFound = false
		for i, v in ipairs(actionMenu.selOptions) do
			if v.unit.type == primary then
				return self:selectTarget(actionMenu,primary)
			elseif v.unit.type == secondary then
				secondaryFound = true
			end
		end
		if secondaryFound then
			return self:selectTarget(actionMenu,secondary)
		else
			return self:selectTarget(actionMenu,tertiary)
		end	
	end,
	
	selectTarget = function(self,actionMenu,unitType)
		if actionMenu.selOptions[actionMenu.curSelOption].unit.type == unitType then
			actionMenu:keypressed("return",self)
		else
			actionMenu:keypressed("down",self)
		end
	end,
	
	isActionAvailible = function(self,actionMenu,item)
		for i,v in ipairs(actionMenu.options) do
			if v == item then
				return true
			end
		end
		return false
	end,
	
	getNearestScrap = function(self,unit,game)
		local nearestScrap = nil
		local distance = 1000000
		for i, scrap in ipairs(game.map:getScrapLocations()) do
			local path = game.map:getPath(unit.x,unit.y,scrap.x,scrap.y,false,unit.type ~= "infantry")
			if path then
				local dis = path:getLength()
				if (not nearestScrap) or dis < distance then
					nearestScrap = scrap
					distance = dis
				end
			end
		end
		return nearestScrap
	end,
	
	getNearestCapturableBase = function(self,unit,game)
		local nearestBase = nil
		local distance = 1000000
		for i, base in ipairs(game.map:getBaseLocations()) do
			if not self:hasBaseAt(base.x, base.y) then
				local path = game.map:getPath(unit.x,unit.y,base.x,base.y,false,unit.type ~= "infantry")
				if path then
					local dis = path:getLength()
					if (not nearestBase) or dis < distance then
						nearestBase = base
						distance = dis
					end
				end
			end
		end
		return nearestBase,distance
	end,
	
	getNearestFriendlyBase = function(self,unit,game)
		local nearestBase = nil
		local distance = 1000000
		for i, base in ipairs(self.bases) do
			local path = game.map:getPath(unit.x,unit.y,base.x,base.y,false,unit.type ~= "infantry")
			if path then
				local dis = path:getLength()
				if (not nearestBase) or dis < distance then
					nearestBase = base
					distance = dis
				end
			end
		end
		return nearestBase
	end,		
	
	selectUnit = function(self,unit,game,dt)
		if game.selection.x == self.currentUnit.x and game.selection.y == self.currentUnit.y then
			game:performPrimaryActionOnTile(self,game.selection.x,game.selection.y)
			--hackyFix
			if unit ~= game.selectedUnit and (not game.selectedUnit) then
				table.remove(self.unitsToMove,1)
				self.currentUnit = nil
				self.moveLocation = nil
				game.selectedUnit = nil
			end
		else
			self.cursorSpeed = self.selectCursorSpeed
			self:moveCursor(game,self.currentUnit.x,self.currentUnit.y,dt)
		end
	end,

	setRandomPosition = function(self,unit,game)
		self.moveLocation = self:getRandomMove(unit,game)
	end,
	
	setNearestPositon = function(self,unit,game,x,y)
		self.moveLocation = self:getMoveTowards(unit,game,x,y)
	end,
	
	setNearestToScrap = function(self,unit,game)
		local nearestScrap = self:getNearestScrap(unit,game)
		if nearestScrap then
			self.moveLocation =  self:getMoveTowards(unit,game,nearestScrap.x,nearestScrap.y)
		else
			self.moveLocation = vector(unit.x,unit.y)
		end
	end,
	
	trySetNearestToCapturableBase = function(self,unit,game)
		local nearestBase = self:getNearestCapturableBase(unit,game)
		if nearestBase then
			self.moveLocation =  self:getMoveTowards(unit,game,nearestBase.x,nearestBase.y)
			return true
		else
			return false
		end
	end,
	
	moveIfWithinRangeCapturableBase = function(self,unit,game)
		local nearestBase,distance = self:getNearestCapturableBase(unit,game)
		if nearestBase and distance <= unit.moveRange then
			self.moveLocation = {x = nearestBase.x, y=nearestBase.y}
			return true
		else
			return false
		end
	end,
	
	setNearestToFriendlyBase = function(self,unit,game)
		local nearestBase = self:getNearestFriendlyBase(unit,game)
		if nearestBase then
			self.moveLocation =  self:getMoveTowards(unit,game,nearestBase.x,nearestBase.y)
		else
			self.moveLocation = vector(unit.x,unit.y)
		end
	end,
	
	setNearestToEnemy = function(self,unit,game,unitType)
		local nearestEnemy = self:getNearestEnemy(unit,game,unitType)
		if nearestEnemy then
			local target =  self:getMoveTowards(unit,game,nearestEnemy.x,nearestEnemy.y,true)
			if game:findUnit(target.x, target.y)then
				self.moveLocation = vector(unit.x,unit.y)
			else
				self.moveLocation = target
			end	
		else
			self.moveLocation = vector(unit.x,unit.y)
		end
	end,
	
	trySetWithinRangeNearestEnemy = function(self,unit,game,unitType)
		local nearestEnemy = self:getNearestEnemy(unit,game,unitType)
		if nearestEnemy then
			self.moveLocation =  self:getMoveWithinRange(unit,game,nearestEnemy.x,nearestEnemy.y)
			return true
		else
			return false
		end
	end,
	
	
	
	
	getRandomMove = function(self,unit,game)
		local  posibleMoves = game:getFloodfill(unit.x,unit.y,unit.moveRange, false, unit.type ~= "infantry")
		local location =  posibleMoves[love.math.random(1,#posibleMoves)]
		return {x = location.x, y = location.y}
	end,
	
	moveToMoveLocation = function(self,unit,game,dt)
		if game.selection.x == self.moveLocation.x and game.selection.y == self.moveLocation.y then
			game:performPrimaryActionOnTile(self,game.selection.x,game.selection.y)
		else 
			self.cursorSpeed = self.moveCursorSpeed
			self:moveCursor(game,self.moveLocation.x,self.moveLocation.y,dt)
		end
	end,
	
	
	getMoveTowards = function(self,unit,game,x,y,endInFront)
		local distanceTraveled = 0
		local path
		if endInFront then
			path = game.map:getPath(unit.x,unit.y,x,y,false, unit.type ~= "infantry",{x=x,y=y})
		else
			path = game.map:getPath(unit.x,unit.y,x,y,false, unit.type ~= "infantry")
		end
		if path then
			local nodes = {}
			for node, count in path:nodes() do
				table.insert(nodes,vector(node:getX(),node:getY()))
			end
			for i,v in ipairs(nodes) do
				if i > 1 then
					local segmentLength = nodes[i-1]:dist(v)
					if distanceTraveled + segmentLength ==  unit.moveRange then
						if i == #nodes and endInFront then
							local direction = v - nodes[i-1]
							direction:normalize_inplace()	
							return v - direction
						else
							return v
						end
					elseif distanceTraveled + segmentLength < unit.moveRange then
				
						distanceTraveled = distanceTraveled + segmentLength
					else
						local direction = v - nodes[i-1]
						direction:normalize_inplace()
						direction = direction * (unit.moveRange - distanceTraveled)
						local endPoint = nodes[i-1] + direction
						return vector(math.floor(endPoint.x+0.5),math.floor(endPoint.y+0.5))	
					end
				end
			end
			if #nodes > 1 then
				if endInFront then
					local direction = nodes[#nodes] - nodes[#nodes -1]
					direction:normalize_inplace()	
					return nodes[#nodes] - direction
				else
					return nodes[#nodes]
				end
			end		
		end
		return vector(unit.x,unit.y)
	end,
	
	getMoveWithinRange = function(self,unit,game,x,y)
		local weaponRange = unit.weapons[1]
		weaponRange = weaponRange and weaponRange.range or 0
		local distanceTraveled = 0
		local path = game.map:getPath(unit.x,unit.y,x,y,false, unit.type ~= "infantry",{x=x,y=y})
		if path then
			local nodes = {}
			local tiles = {}
			for node, count in path:nodes() do
				table.insert(nodes,vector(node:getX(),node:getY()))
			end
			table.insert(tiles,{x=unit.x,y=unit.y})
			for i,v in ipairs(nodes) do
				if i > 1 then
					if nodes[i-1].x == v.x then
						if v.y  > nodes[i-1].y then
							for j = nodes[i-1].y+1 , v.y do
								table.insert(tiles,{x=v.x,y=j})
							end
						else
							for j = nodes[i-1].y-1, v.y, -1 do
								table.insert(tiles,{x=v.x,y=j})
							end
						end
					else
						if v.x  > nodes[i-1].x then
							for j = nodes[i-1].x+1 , v.x do
								table.insert(tiles,{x=j,y=v.y})
							end
						else
							for j = nodes[i-1].x-1 , v.x, -1 do
								table.insert(tiles,{x=j,y=v.y})
							end
						end
					end
				end
			end
			for i = 1, weaponRange do
				if #tiles > 1 then
					table.remove(tiles,#tiles)
				end
			end
			
			return tiles[math.min(#tiles,unit.moveRange+1)]	
		end
		return vector(unit.x,unit.y)
	end,
	
	getNearestEnemy = function(self,unit,game,unitType)
		local nearestEnemy = nil
		local distance = 1000000
		for j, player in ipairs(game.players) do
			if player ~= self then
				for i, enemy in player:iterUnits() do
					if (not unitType) or enemy.type == unitType then
						local path = game.map:getPath(unit.x,unit.y,enemy.x,enemy.y,true,unit.type ~= "infantry")
						if path then
							local dis = path:getLength()
							if (not nearestEnemy) or dis < distance then
								nearestEnemy = enemy
								distance = dis
							end
						end
					end
				end
			end
		end
		return nearestEnemy
	end,
	
	
	

	moveCursor = function(self,game,x,y,dt)
		local destination = vector(x,y)
		local direction = destination -  self.cursorPosition	
		local directionNormalised = direction:normalized()
		direction = directionNormalised *  self.cursorSpeed * dt	
		self.cursorPosition = self.cursorPosition + direction
		local newDirection = destination - self.cursorPosition
		newDirection:normalize_inplace()
		local diffx = directionNormalised.x - newDirection.x
		local diffy = directionNormalised.y - newDirection.y 			
		if diffx > 0.1 or diffx < -0.1 or diffy > 0.1 or diffy < -0.1  then
			self.cursorPosition = destination
		end
		game:moveSelectionCursor(self,math.floor(self.cursorPosition.x+0.5),math.floor(self.cursorPosition.y+0.5))
	end,
	
	setBasesToUpdate = function(self,game)
		self.basesToUpdate = {}
		local scrap = self.scrap
		if scrap < 800 and love.math.random(1,100) <50 then
			return -- save up for tank 
		end
		for i,base in ipairs(self.bases) do
			if not game:findUnit(base.x, base.y)then
				local unit = self:getUnitSelection(scrap)
				if unit then
					table.insert(self.basesToUpdate,{base = base, unit = unit})
					scrap = scrap - unit.cost
				end
				
			end
		end
	end,
	
	getUnitSelection = function(self,scrap)
		local possibleUnits = {} 
		local truckCount = self:countUnitsOfType("Truck")
		for i,unit in ipairs(unitSelection) do
			if unit.cost < scrap and (truckCount > 1 or unit.__name__ == "Truck") then
				local count = self:countUnitsOfType(unit.__name__)
				local weighting =  math.ceil(#self.units/math.max(count,1))
				if unit.__name__ == "Tank" or unit.__name__ == "Jeep" then
					weighting = weighting * 2
				end
				if unit.__name__ == "Truck" then
					weighting = weighting * 3
				end
				for j=1,weighting do
					table.insert(possibleUnits,unit)
				end
			end
		end
		if #possibleUnits > 0 then
			return possibleUnits[love.math.random(1,#possibleUnits)]
		else
			return false
		end
	end,
	
	countUnitsOfType = function(self,unitType)
		local count = 0
		for i,unit in ipairs(self.units) do
			if unit.__name__ == unitType then
				count = count + 1
			end
		end
		return count
	end,
	
	startTurn = function(self,game)
		
		self:setBasesToUpdate(game)
		self.unitsToMove = {}
		for i, v in ipairs(self.units) do
			if self:hasBaseAt(v.x,v.y) then
				table.insert(self.unitsToMove,1,v)
			else
				table.insert(self.unitsToMove,math.max(#self.unitsToMove,1),v)
			end
		end
		self.cursorPosition = vector(game.selection.x,game.selection.y)		
	end,

	killUnit = function(self, unit)
		if unit == self.currentUnit then
			self.currentUnit = nil
			table.remove(self.unitsToMove,1)
			self.moveLocation = nil
		end
		return Player.killUnit(self, unit)
	end,
}
