local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"
local newAnimation = require "lib.AnAL"
local actionmenu = require "state.actionmenu"
local dialog = require "state.dialog"
local buymenu = require "state.buymenu"
local turnmenu = require "state.turnmenu"
local vector = require "lib.hump.vector"

local game = {}

gridSize = 32
local w, h
local uiX, uiY
local unit1X, unit1Y, unit2X, unit2Y = 148, 672, 728, 672
local ww, wh

local function screenToGrid(x, y)
	return math.floor(x/gridSize)+1, math.floor(y/gridSize)+1
end

local function toGridCenter(x)
	return (x-0.5)*gridSize
end

local function drawRectOnTile(type, x, y)
	return love.graphics.rectangle(type, (x-1)*gridSize, (y-1)*gridSize, gridSize, gridSize)
end

function game:init()
	local cursorImg = cache.image("gfx/cursor.png")
	self.cursor = newAnimation(cursorImg, 33, 33, 0.4, 0)

	self.withgrid = false
	self.uiFont = cache.font("font/wendy.ttf:20")
	self.turnFont = cache.font("font/MEGA!.ttf:16")

	self.blueSelectionTile = cache.image("gfx/highlightBlue.png")

	self.ui = {}
	self.ui.bg = cache.image("gfx/ui/background.png")
	self.ui.info = cache.image("gfx/ui/unitCards/info.png")
	for i, v in ipairs{"Jeep", "ReconSquad", "RushSquad", "SupportSquad", "Tank", "TechSquad", "Truck"} do
		self.ui[v] = cache.image("gfx/ui/unitCards/" .. v .. ".png")
	end

	self.musics =
	{
		[1] = cache.music("sfx/ld31b-1.ogg"),
		[2] = cache.music("sfx/ld31c.ogg"),
	}

	for _, music in ipairs(self.musics) do
		music:setVolume(0)
		music:setLooping(true)
		music:play()
	end

	self.musics[0] = self.musics[#self.musics]
end

function game:enter()
	local mapdata = love.filesystem.load("newmap.lua")()
	w, h = mapdata.width, mapdata.height
	gridSize = mapdata.tilewidth
	uiX, uiY = 0, h * gridSize
	ww, wh = love.graphics.getDimensions()

	self.map = Map(mapdata, self:createUnitIter())
	self.players = {
		HumanPlayer(),
		AiPlayer(),
	}

	for i, v in ipairs(self.map.ownedBases) do

		table.insert(self.players[v[3]].bases, {x = v[1], y = v[2]})
		self.players[v[3]].homeBase = self.players[v[3]].bases[#self.players[v[3]].bases]
	end
	--33 8
	self.players[1]:addUnit(Truck(3, 11, self.map))
	self.players[1]:addUnit(RushSquad(3, 13, self.map))
	self.players[2]:addUnit(Truck(33, 9, self.map))
	self.players[2]:addUnit(RushSquad(33, 7, self.map))
	
	self.currentPlayer = 1
	
	self.selectedUnit = nil
	self.selection = {x = 0, y = 0}
	self.floodFilled = {}
	self.oldPos = {x = -1, y = -1}
	self.oldRange = 0
	self.oldMousePos = {x = -1, y = -1}
	self.arrows = {}
	self.turnCounter = 1
	self.finished = false

	self.musics[self.currentPlayer]:setVolume(1)
	self.musics[1]:rewind()
	self.musics[2]:rewind()

	love.keyboard.setKeyRepeat(true)
end

function game:createUnitIter()
	local function iterfunc()
		for _, player in ipairs(self.players) do
			for i, v in player:iterUnits() do
				coroutine.yield(v)
			end
		end
	end

	return function()
		return coroutine.wrap(iterfunc)
	end
end

function game:leave()
	for _, music in ipairs(self.musics) do
		music:setVolume(0)
	end

	self.map = nil
	self.players = nil
	self.selectedUnit = nil
end

function game:actionFinished()
	self.selectedUnit = nil
	local dTitle, dText
	for _, player in ipairs(self.players) do
		if DEBUGGING then -- FIXME
			player.homeBase = true
		end
		if (player:getUnitCount() == 0 or not player.homeBase) and player.alive then
			player.alive = false
			dTitle, dText = "Kill", "Player deaded"
			break
		end
	end

	local alive = 0
	for _, player in ipairs(self.players) do
		if player.alive then
			alive = alive + 1
		end
	end

	if alive == 1 then
		dTitle, dText = "Win", "A winner is you"
		self.finished = true
	end

	if dTitle then
		Gamestate.push(dialog, dTitle, dText)
	end
end

function game:resume()
	if self.finished then
		Gamestate.switch(require "state.menu")
	end
end

function game:validSelection(selection)
	return self.selectedUnit and math.abs(selection.x - self.selectedUnit.x) + math.abs(selection.y - self.selectedUnit.y) <= self.selectedUnit.moveRange
end

function game:nextPlayersTurn()
	self.musics[self.currentPlayer%#self.musics]:setVolume(0)
	self.currentPlayer = (self.currentPlayer  % #self.players) + 1
	self.musics[self.currentPlayer%#self.musics]:setVolume(1)
	self.musics[self.currentPlayer%#self.musics]:rewind()
	if self.currentPlayer == 1 then
		self.turnCounter = self.turnCounter + 1
	end
	if self.players[self.currentPlayer].alive then
		self:startTurn()
		Gamestate.push(dialog, "New turn", ("Player %d, turn %d"):format(self.currentPlayer, self.turnCounter), 1.5)
	else
		return self:nextPlayersTurn()
	end
end

function game:startTurn()
	for _, player in ipairs(self.players) do
		for i, v in player:iterUnits() do
			v.turnComlete = false
		end
	end
	
	self.selectedUnit = nil
	self.oldPos = {x = -1, y = -1} --force update of floodfill
	
	for i, v in self.players[self.currentPlayer]:iterUnits() do
		v:startTurn()
	end
	self.players[self.currentPlayer]:startTurn(self)
	
end

function game:keypressed(key)
	if key == "left" then
		self.players[self.currentPlayer]:userInputMovement(self, self.selection.x-1,self.selection.y)
	elseif key == "right" then
		self.players[self.currentPlayer]:userInputMovement(self, self.selection.x+1,self.selection.y)
	elseif key == "up" then
		self.players[self.currentPlayer]:userInputMovement(self, self.selection.x,self.selection.y-1)
	elseif key == "down" then
		self.players[self.currentPlayer]:userInputMovement(self, self.selection.x,self.selection.y+1)
	end

	if key == "return" then
		self.players[self.currentPlayer]:userPrimaryAction(self, self.selection.x, self.selection.y)
	end

	if key == "escape" then
		if self.currentPlayer == 1 and self.selectedUnit then
			self.selectedUnit = nil
		else
			Gamestate.push(turnmenu)
		end
	end

	if key == "g" then
		self.withgrid = not self.withgrid
	end
	
	if DEBUGGING then
		if key == "p" then
			self:nextPlayersTurn()
		elseif key == "a" and self.selectedUnit then
			Gamestate.push(actionmenu, self.selectedUnit, self.map, self.players[self.currentPlayer])
		end
	end
end

function game:findUnitForPlayer(player, x, y)
	for i, v in player:iterUnits() do
		if v.x == x and v.y == y then
			return v
		end
	end
end

function game:findUnit(x, y)
	for i, v in ipairs(self.players) do
		local unit = self:findUnitForPlayer(v, x, y)
		if unit then return unit, v end
	end
end

function game:performPrimaryActionOnTile(player,x,y)
	if player ~= self.players[self.currentPlayer] then
		return
	end

	self.arrows = {}
	
	local unit, owner = self:findUnit(x, y)
	
	if self.selectedUnit then
		if not self.selectedUnit.previousPosition then
			self.selectedUnit:move(x,y)
		end
	elseif unit then
		if owner == player and not unit.previousPosition and not unit.actionFinished then
			self.selectedUnit = unit
			self.selection = {x = unit.x, y = unit.y}
		end
	elseif player:hasBaseAt(x, y) then
		Gamestate.push(buymenu, player, vector(x, y))
	end
end

function game:performAttack(unit, owner)
	local path = self.map:getPath(self.selectedUnit.x, self.selectedUnit.y, unit.x, unit.y, true)
	local weaponRange = self.selectedUnit.weapons[1]
	weaponRange = weaponRange and weaponRange.range or 0

	if path:getLength() <= weaponRange then
		local dmg = self.selectedUnit:attack(unit)
		unit.hp = unit.hp - dmg
		if unit.hp <= 0 then
			owner:killUnit(unit)
		elseif path:getLength() == 1 then
			local dmg = unit:counterAttack(self.selectedUnit, self.players[self.currentPlayer])
			self.selectedUnit.hp = self.selectedUnit.hp - dmg
			if self.selectedUnit.hp <= 0 then
				self.players[self.currentPlayer]:killUnit(self.selectedUnit)
				self.selectedUnit = nil
			end
		end
	end
end

function game:performSecondaryActionOnTile(player,x,y)
	if player ~= self.players[self.currentPlayer] then
		return
	end

	if self.selectedUnit then
		self.selectedUnit = nil
	end
end

function game:moveSelectionCursor(player,x,y)
	if player == self.players[self.currentPlayer] and self.map:inRange(x,y) then
		self.targetedUnit = self:findUnit(x, y)
		self.selection.x,self.selection.y = x,y
		self.arrows = {}
		if self.selectedUnit then
			local path = self.map:getPath(self.selectedUnit.x,self.selectedUnit.y,x,y, false, self.selectedUnit.type ~= "infantry")
			if path then
				for node, count in path:nodes() do
					table.insert(self.arrows,toGridCenter(node:getX()))
					table.insert(self.arrows,toGridCenter(node:getY()))
				end
			end
		end
	end
end

function game:mousepressed(x, y, button)
	if button == 'l' then
		self.players[self.currentPlayer]:userPrimaryAction(self, screenToGrid(x, y))
	elseif button == 'r' then
		self.players[self.currentPlayer]:userSecondaryAction(self, screenToGrid(x, y))
	end
end

local function floodFill(x, y, range, map, ignoreUnits, vehicleMovement)
	local tiles = {}
	local openlist = {{x, y, range}}
	local marked = {}

	local function ismarked(x, y)
		return marked[y] and marked[y][x]
	end

	local function mark(x, y)
		if not marked[y] then marked[y] = {} end
		marked[y][x] = true
	end

	local occupiedT = {}
	for v in map.unitIter() do
		occupiedT[v.y] = occupiedT[v.y] or {}
		occupiedT[v.y][v.x] = true
	end

	local function occupied(mx, my)
		if ignoreUnits then return false end
		if my == y and mx == x then return false end
		if not occupiedT[my] then return false end
		return occupiedT[my][mx]
	end

	local property = vehicleMovement and "driveable" or "walkable"

	while #openlist > 0 do
		local t = table.remove(openlist, 1)
		if map:inRange(t[1], t[2]) and not ismarked(t[1], t[2]) and map:isPassable(t[1], t[2],vehicleMovement) and not occupied(t[1], t[2]) then
			table.insert(tiles, vector(t[1], t[2]))
			mark(t[1], t[2])
			if t[3] > 0 then
				local r = t[3]-1
				table.insert(openlist, {t[1]-1, t[2], r})
				table.insert(openlist, {t[1]+1, t[2], r})
				table.insert(openlist, {t[1], t[2]-1, r})
				table.insert(openlist, {t[1], t[2]+1, r})
			end
		end
	end

	table.remove(tiles, 1)
	return tiles
end

function game:getFloodfill(x,y,range, ignoreUnits, vehicleMovement)
	return floodFill(x, y, range, self.map, ignoreUnits, vehicleMovement)
end

function game:update(dt)
	local x,y = love.mouse.getPosition()
	if self.oldMousePos.x ~= x or self.oldMousePos.y ~= y then 
		self.oldMousePos.x , self.oldMousePos.y = x,y
		self.players[self.currentPlayer]:userInputMovement(self, screenToGrid(x, y))
	end
	
	self.players[self.currentPlayer]:update(self, dt)
	self.cursor:update(dt)

	-- Update units
	for _, player in ipairs(self.players) do
		for i, v in player:iterUnits() do
			v:update(dt)
		end
	end
	
	--open action menu when movement completed
	if self.selectedUnit and self.selectedUnit.previousPosition and #self.selectedUnit.pathNodes == 0 and not self.selectedUnit.actionSelected then
		self.arrows = {}
		Gamestate.push(actionmenu, self.selectedUnit, self.map, self.players[self.currentPlayer])
		self.selectedUnit.actionSelected = true
	end
end

local function daleIsLazySoPrintRightAlignedWithDropShadow(font, str, rightx, y)
	x = rightx-font:getWidth(str)
	love.graphics.setColor(0, 0, 0, 191)
	love.graphics.print(str, x+1, y+1)
	love.graphics.setColor(255, 255, 255)
	love.graphics.print(str, x, y)
end

function game:drawUnitUI(xoff, unit)
	if not self.ui[unit.__name__] then return end
	love.graphics.draw(self.ui.info, xoff, uiY)
	love.graphics.draw(self.ui[unit.__name__], xoff, uiY)
	love.graphics.setFont(self.uiFont)
	daleIsLazySoPrintRightAlignedWithDropShadow(self.uiFont, unit.hp, xoff+48, uiY+26)
	local def = 10-self.map:getAccuracyMultiplier(unit.x, unit.y)*10
	daleIsLazySoPrintRightAlignedWithDropShadow(self.uiFont, def, xoff+103, uiY+26)
end

function game:drawUI()
	love.graphics.draw(self.ui.bg, uiX, uiY)
	love.graphics.setFont(self.uiFont)
	daleIsLazySoPrintRightAlignedWithDropShadow(self.uiFont, self.players[self.currentPlayer].scrap, uiX+77, uiY+26)

	love.graphics.setFont(self.turnFont)
	daleIsLazySoPrintRightAlignedWithDropShadow(self.turnFont, self.turnCounter, uiX+121, uiY+9)
end

function game:draw()
	-- Draw map
	self.map:draw(self.players)

	-- Draw grid
	if self.withgrid then
		love.graphics.setColor(0, 0, 0)
		local rw, rh = w*gridSize, h*gridSize
		love.graphics.setLineStyle("rough")
		for x = 0, rw, gridSize do
			love.graphics.line(x, 0, x, rh)
		end
		for y = 0, rh, gridSize do
			love.graphics.line(0, y, rw, y)
		end
	end

	if self.selectedUnit then
		-- Draw range of selected unit
		love.graphics.setColor(50, 50, 200, 50)
		
		
		local x, y = self.selectedUnit.x, self.selectedUnit.y
		local range = self.selectedUnit.moveRange
		
		if self.selectedUnit.previousPosition then
			if #self.selectedUnit.pathNodes == 0 then
				range = 0
			else
				x,y = self.selectedUnit.previousPosition.x, self.selectedUnit.previousPosition.y
			end	
		end
		local tiles = self.floodFilled
		if self.selectedUnit.x ~= self.oldPos.x or self.selectedUnit.y ~= self.oldPos.y or range ~= self.oldRange then
			self.floodFilled = floodFill(x, y, range, self.map, false, self.selectedUnit.type ~= "infantry")
			tiles = self.floodFilled
			self.oldPos.x = self.selectedUnit.x
			self.oldPos.y = self.selectedUnit.y
			self.oldRange = range
		end
		
		local alpha = 64+44*math.sin(love.timer.getTime())
		love.graphics.setColor(255, 255, 255, alpha)
		for i, v in ipairs(tiles) do
			love.graphics.draw(self.blueSelectionTile, (v.x-1)*gridSize, (v.y-1)*gridSize)
		end

		-- Draw arrow
		love.graphics.setColor(200, 50, 50)
		if #self.arrows > 2 then
			love.graphics.line(self.arrows)
		end
	end

	-- Draw units
	for j, player in ipairs(self.players) do
		for i, v in player:iterUnits() do
			love.graphics.setColor(player:getColor())
			v:draw(j%2 == 0)
		end
	end

	-- Draw selection box thingy
	do
		local unit, owner = self:findUnit(self.selection.x, self.selection.y)
		if unit then
			if owner == self.players[self.currentPlayer] then
				if unit:hasMoved() then
					love.graphics.setColor(100, 150, 100)
				else
					love.graphics.setColor(150, 255, 150)
				end
			else
				love.graphics.setColor(255, 150, 150)
			end
		elseif self.players[self.currentPlayer]:hasBaseAt(self.selection.x, self.selection.y) then
			love.graphics.setColor(150, 255, 150)
		else
			love.graphics.setColor(255, 255, 255)
		end
	end
	self.cursor:draw((self.selection.x-1)*gridSize, (self.selection.y-1)*gridSize)

	-- Draw ui
	love.graphics.setColor(255, 255, 255)
	self:drawUI()
	if self.selectedUnit then
		self:drawUnitUI(unit1X, self.selectedUnit)
	end
	if self.targetedUnit and self.selectedUnit ~= self.targetedUnit then
		local x = unit2X
		if not self.selectedUnit and self.players[self.currentPlayer]:ownsUnit(self.targetedUnit) then
			x = unit1X
		end
		self:drawUnitUI(x, self.targetedUnit)
	end
end

function game:captureBase(player, x, y)
	print("Capturing base")
	for _, player in ipairs(self.players) do
		for i, v in ipairs(player.bases) do
			if v.x == x and v.y == y then
				table.remove(player.bases, i)
				if player.homeBase == v then
					player.homeBase = nil
				end
				break
			end
		end
	end

	table.insert(player.bases, {x = x, y = y})
end

return game
