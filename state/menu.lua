local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"
local game = require "state.game"
local help = require "state.help"

local menu = {}

local options =
{
	{
		text = "New game",
		img = cache.image("gfx/ui/title/start.png"),
		cb = function()
			menuMusic:stop()
			return Gamestate.switch(game)
		end
	},

	{
		text = "How to play",
		img = cache.image("gfx/ui/title/help.png"),
		cb = function()
			return Gamestate.push(help)
		end
	},

	{
		text = "Exit",
		img = cache.image("gfx/ui/title/quit.png"),
		cb = function()
			menuMusic:stop()
			return love.event.quit()
		end
	},
}

function menu:init()
	self.font = cache.font("font/wendy.ttf:20")
end

function menu:enter()
	menuMusic:play()
	self.selection = 1
end

function menu:keypressed(key)
	if key == "return" then
		options[self.selection].cb()
	elseif key == "escape" then
		love.event.quit()
	elseif key == "up" then
		self.selection = (self.selection - 2) % #options + 1
	elseif key == "down" then
		self.selection = self.selection % #options + 1
	end
end

function menu:draw()
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(options[self.selection].img)
end

return menu
