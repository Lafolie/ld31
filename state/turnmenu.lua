local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"

local turnmenu = {}

local options =
{
	{ text = "End turn", cb = function(self)
		Gamestate.pop()
		return self.previous:nextPlayersTurn()
	end },
	{ text = "Quit to title", cb = function()
		Gamestate.pop()
		return Gamestate.switch(require "state.menu")
	end },
	{ text = "Close menu", cb = function()
		return Gamestate.pop()
	end },
}

function turnmenu:init()
	self.font = cache.font("font/wendy.ttf:20")
	self.bg = cache.image("gfx/ui/menu.png")
	self.selectorImg = cache.image("gfx/ui/menuSelector.png")
end

function turnmenu:enter(previous, player, position)
	self.previous = previous
	self.selected = 1
end

function turnmenu:keypressed(key)
	if key == "return" then
		return options[self.selected].cb(self)
	elseif key == "escape" then
		return Gamestate.pop()
	elseif key == "up" then
		self.selected = (self.selected - 2) % #options + 1
	elseif key == "down" then
		self.selected = self.selected % #options + 1
	end
end

function turnmenu:draw()
	self.previous:draw()

	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg, 50, 50)
	love.graphics.setFont(self.font)

	for i, v in ipairs(options) do
		local x = 63
		if i == self.selected then
			love.graphics.draw(self.selectorImg, x-2, 51+17*i)
			x = x + 2*math.sin(love.timer.getTime())
		end
		love.graphics.print(v.text, x, 43+17*i)
	end
end

return turnmenu
