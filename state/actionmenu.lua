local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"
local vector = require "lib.hump.vector"
local newAnimation = require "lib.AnAL"

local actionmenu = {}
local actions = {}
local surroundings =
{
	vector(-1, 0),
	vector( 1, 0),
	vector(0, -1),
	vector(0,  1),
}

local possibleOptions = {"Attack", "Heal", "Repair", "Embark", "Disembark", "Salvage", "Unload", "Capture", "Wait", "Undo"}

function actions.Attack(self)
	local targets = self.optionDetails["Attack"]
	local range = self.unit.weapons[1].range
	return self:doSelection(targets, range, function(target)
		self.previous:performAttack(target.unit, target.owner)
		self.unit.turnComlete = true
		return Gamestate.pop()
	end)
end

function actions.Embark(self)
	local targets = self.optionDetails["Embark"]
	return self:doSelection(targets, 1, function(target)
		if target.unit:embark(self.unit) then
			self.player:removeUnit(self.unit)
		end
		self.unit.turnComlete = true
		return Gamestate.pop()
	end)
end

function actions.Heal(self)
	local targets = self.optionDetails["Heal"]
	return self:doSelection(targets, 1, function(target)
		self.unit:heal(target.unit)
		self.unit.turnComlete = true
		return Gamestate.pop()
	end)
end

function actions.Repair(self)
	local targets = self.optionDetails["Repair"]
	return self:doSelection(targets, 1, function(target)
		self.unit:repair(target.unit)
		self.unit.turnComlete = true
		return Gamestate.pop()
	end)
end

function actions.Disembark(self)
	local targets = self.optionDetails["Disembark"]
	return self:doSelection(targets, 1, function(target)
		local unit = self.unit:disembark()
		if unit then
			unit.x, unit.y = target.unit.x, target.unit.y
			unit.currentPosition = vector(unit.x, unit.y)
			self.player:addUnit(unit)
		end
		self.unit.turnComlete = true
		return Gamestate.pop()
	end, true)
end

function actions.Salvage(self)
	local salvaged = self.unit:salvage(self.optionDetails["Salvage"])
	self.map:salvage(self.unit.x, self.unit.y, salvaged)
	print("Salvaged ", salvaged, " units of scrap")
	self.unit.turnComlete = true
	return Gamestate.pop()
end

function actions.Unload(self)
	local scrap = self.unit.carrying.scrap
	self.unit.carrying.scrap = 0
	self.player.scrap = self.player.scrap + scrap
	self.unit.turnComlete = true
	return Gamestate.pop()
end

function actions.Capture(self)
	self.previous:captureBase(self.player, self.unit.x, self.unit.y)
	self.unit.turnComlete = true
	return Gamestate.pop()
end

function actions.Wait(self)
	self.unit.turnComlete = true
	return Gamestate.pop()
end

function actions.Undo(self)
	self.unit:undoMovement()
	return Gamestate.pop()
end

function actionmenu:init()
	self.font = cache.font("font/wendy.ttf:20")
	local cursorImg = cache.image("gfx/cursor.png")
	self.cursor = newAnimation(cursorImg, 33, 33, 0.4, 0)

	self.redSelectionTile = cache.image("gfx/highlightRed.png")
	self.bg = cache.image("gfx/ui/menu.png")
	self.selectorImg = cache.image("gfx/ui/menuSelector.png")
end

function actionmenu:enter(previous, unit, map, player)
	self.previous, self.unit, self.map, self.player = previous, unit, map, player
	self.pos = {x = unit.x*gridSize + 10, y = (unit.y-1)*gridSize}
	if self.pos.x > 1280-160 then
		self.pos.x = self.pos.x - 160 - 20 - gridSize
	end
	if self.pos.y > 720-180 then
		self.pos.y = self.pos.y - 180
	end

	self.options = {}
	self.optionDetails = {}
	self.mapPos = vector(unit.x, unit.y)
	self:addOptions()

	if #self.options == 0 then
		Gamestate.pop()
	end

	self.currentOption = 1
	self.selection = nil
end

function actionmenu:leave()
	self.previous.targetedUnit = nil
	self.previous:actionFinished()
end

function actionmenu:addOptions()
	local opts = {["Wait"] = true, ["Undo"] = true}
	
	for i, v in ipairs(surroundings) do
		local pos = self.mapPos+v
		local unit, owner = self.previous:findUnit(pos.x, pos.y)
		if unit and owner == self.player and unit:canEmbark(self.unit) then
			opts["Embark"] = opts["Embark"] or {}
			table.insert(opts["Embark"], {unit = unit})
		end
	end
	
	for i, v in ipairs(surroundings) do
		local pos = self.mapPos+v
		local unit, owner = self.previous:findUnit(pos.x, pos.y)
		if unit and owner == self.player and  self.unit:canHeal(unit)then
			opts["Heal"] = opts["Heal"] or {}
			table.insert(opts["Heal"], {unit = unit})
		end
	end
	
	for i, v in ipairs(surroundings) do
		local pos = self.mapPos+v
		local unit, owner = self.previous:findUnit(pos.x, pos.y)
		if unit and owner == self.player and  self.unit:canRepair(unit)then
			opts["Repair"] = opts["Repair"] or {}
			table.insert(opts["Repair"], {unit = unit})
		end
	end

	local range = self.unit.weapons[1] and self.unit.weapons[1].range or 0
	for i, pos in ipairs(self.previous:getFloodfill(self.unit.x, self.unit.y, range, true)) do
		local unit, owner = self.previous:findUnit(pos.x, pos.y)
		if unit and owner ~= self.player and self.unit.weapons[1] then
			opts["Attack"] = opts["Attack"] or {}
			table.insert(opts["Attack"], {unit = unit, owner = owner})
		end
	end

	local scrap = self.map:getScrap(self.unit.x, self.unit.y)
	if scrap > 0 and self.unit:canSalvage() then
		opts["Salvage"] = scrap
	end

	if self.unit.carrying.scrap > 0 and self.player:hasBaseAt(self.unit.x, self.unit.y) then
		opts["Unload"] = true
	end

	if self.unit.carrying.infantry > 0 then
		local targets = {}
		for i, v in ipairs(self.previous:getFloodfill(self.unit.x, self.unit.y, 1)) do
			table.insert(targets, {unit = Soldier(v.x, v.y)})
		end
		if #targets > 0 then
			opts["Disembark"] = targets
		end
	end

	local tile = self.map:getTile(self.unit.x, self.unit.y)
	if self.unit.type == "infantry" and not self.player:hasBaseAt(self.unit.x, self.unit.y) and tile.properties.base then
		opts["Capture"] = true
	end

	for i, v in ipairs(possibleOptions) do
		if opts[v] then
			table.insert(self.options, v)
		end
	end
	self.optionDetails = opts
end

function actionmenu:doSelection(options, range, cb, disallowUnits)
	self.selection = self.previous:getFloodfill(self.unit.x, self.unit.y, range, not disallowUnits)
	self.selOptions = options
	self.curSelOption = 1
	self.selCb = cb
end

function actionmenu:keypressed(key,player)
	if self.player.__name__ == "AiPlayer" and ((not player) or player ~= self.player) then
		return
	end
	
	if key == "escape" then
		if self.selection then
			self.selection = nil
		else
			actions.Undo(self)
		end
	end

	if not self.selection then
		if key == "down" then
			self.currentOption = (self.currentOption % #self.options) + 1
		elseif key == "up" then
			self.currentOption = ((self.currentOption - 2) % #self.options) + 1
		end
	else
		if key == "down" then
			self.curSelOption = (self.curSelOption % #self.selOptions) + 1
		elseif key == "up" then
			self.curSelOption = ((self.curSelOption - 2) % #self.selOptions) + 1
		end
	end

	if key == "return" then
		if not self.selection then
			actions[self.options[self.currentOption]](self)
		else
			self.selCb(self.selOptions[self.curSelOption])
		end
	end
end

function actionmenu:update(dt)
	self.cursor:update(dt)
	self.previous.players[self.previous.currentPlayer]:update(self.previous, dt)
	if self.selection then
		self.previous.targetedUnit = self.selOptions[self.curSelOption].unit
	end
end

function actionmenu:draw()
	self.previous:draw()

	if self.selection then
		local alpha = 64+44*math.sin(love.timer.getTime())
		love.graphics.setColor(255, 255, 255, alpha)
		for i, v in ipairs(self.selection) do
			love.graphics.draw(self.redSelectionTile, (v.x-1)*gridSize, (v.y-1)*gridSize)
		end

		local target = self.selOptions[self.curSelOption]
		love.graphics.setColor(200, 50, 50)
		self.cursor:draw((target.unit.x-1)*gridSize, (target.unit.y-1)*gridSize)
	else
		love.graphics.setFont(self.font)
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(self.bg, self.pos.x, self.pos.y)

		for i, v in ipairs(self.options) do
			local x = self.pos.x+13
			if self.currentOption == i then
				love.graphics.draw(self.selectorImg, x-2, self.pos.y+13+20*(i-1))
				x = x + 2*math.sin(love.timer.getTime()*5)
			end

			love.graphics.print(v, x, self.pos.y+5+20*(i-1))
		end
	end
end

return actionmenu
