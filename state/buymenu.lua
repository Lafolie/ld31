local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"

local units =
{
	require "class.Jeep",
	require "class.ReconSquad",
	require "class.RushSquad",
	require "class.SupportSquad",
	require "class.Tank",
	require "class.TechSquad",
	require "class.Truck",
}

local buymenu = {}

buymenu.hack = true

function buymenu:init()
	self.font = cache.font("font/wendy.ttf:20")
	self.bg = cache.image("gfx/ui/menu.png")
	self.selectorImg = cache.image("gfx/ui/menuSelector.png")
end

function buymenu:enter(previous, player, position)
	self.previous = previous
	self.player, self.position = player, position
	self.selected = 1
end

function buymenu:keypressed(key,player)
	if self.player.__name__ == "AiPlayer" and ((not player) or player ~= self.player) then
		return
	end
	if key == "return" then
		if self.selected <= #units then
			local unit = units[self.selected]
			if self.player.scrap >= unit.cost then
				print("Player can afford unit")
				self.player:addUnit(unit(self.position.x, self.position.y, self.previous.map))
				self.player.scrap = self.player.scrap - unit.cost
			else
				print("Player can't afford unit")
			end
		end
		return Gamestate.pop()
	elseif key == "escape" then
		return Gamestate.pop()
	elseif key == "up" then
		self.selected = (self.selected - 2) % (#units + 1) + 1
	elseif key == "down" then
		self.selected = self.selected % (#units + 1) + 1
	end
end

function buymenu:update(dt)
	self.previous.players[self.previous.currentPlayer]:update(self.previous, dt)
end

function buymenu:draw()
	self.previous:draw()

	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg, 50, 50)
	love.graphics.setFont(self.font)

	for i, v in ipairs(units) do
		local x = 63
		if i == self.selected then
			love.graphics.draw(self.selectorImg, x-2, 51+17*i)
			x = x + 2*math.sin(love.timer.getTime())
		end
		love.graphics.print(v.name, x, 43+17*i)
		love.graphics.printf(v.cost, 50, 43+17*i, 152, "right")
	end

	do
		local i = #units+1
		local x = 63
		if i == self.selected then
			love.graphics.draw(self.selectorImg, x-2, 68+17*i)
			x = x + 2*math.sin(love.timer.getTime())
		end
		love.graphics.print("Cancel", x, 60+17*i)
	end
end

return buymenu
