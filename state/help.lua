local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"

local help = {}

function help:init()
	self.helpScreen = cache.image("gfx/ui/title/helpScreen.png")
end

function help:enter(previous)
	self.previous = previous
end

function help:keypressed()
	return Gamestate.pop()
end

function help:draw()
	self.previous:draw()
	love.graphics.draw(self.helpScreen)
end

return help
