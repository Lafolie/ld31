local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"

local dialog = {}

function dialog:init()
	self.titleFont = cache.font("font/wendy.ttf:36")
	self.textFont = cache.font("font/wendy.ttf:20")
end

function dialog:enter(previous, title, text, timeout)
	assert(previous ~= dialog)
	self.previous, self.title, self.text, self.timeout = previous, title, text, timeout or 5
end

function dialog:update(dt)
	self.timeout = self.timeout - dt
	if self.timeout <= 0 then
		Gamestate.pop()
	end
end

function dialog:keypressed(key)
	if key == " " or key == "escape" or key == "return" then
		Gamestate.pop()
	end
end

function dialog:draw()
	self.previous:draw()

	love.graphics.setColor(0, 0, 0)
	love.graphics.rectangle('fill', 440, 260, 400, 200)

	love.graphics.setColor(255, 255, 255)
	love.graphics.setFont(self.titleFont)
	love.graphics.printf(self.title, 450, 265, 380, "center")

	love.graphics.setFont(self.textFont)
	love.graphics.printf(self.text, 450, 300, 380, "left")
end

return dialog
