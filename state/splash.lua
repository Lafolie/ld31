local Gamestate = require "lib.hump.gamestate"
local cache = require "lib.cache"
local menu = require "state.menu"

local splash = {}

function splash:init()
	self.splash = cache.image("gfx/ui/title/title.png")
end

function splash:enter()
	self.timer = 0
	self.runTime = 2
end

function splash:update(dt)
	self.timer = self.timer + dt
	if self.timer >= self.runTime then
		return Gamestate.switch(menu)
	end
end

function splash:keypressed(key)
	return Gamestate.switch(menu)
end

function splash:draw()
	love.graphics.setColor(9, 9, 9)
	love.graphics.rectangle('fill', 0, 0, 1280, 720)

	local a = 0
	a = 1-(self.runTime-self.timer)/self.runTime
	a = 255 * (a*a)

	love.graphics.setColor(255, 255, 255, a)
	love.graphics.draw(self.splash)
end

return splash
