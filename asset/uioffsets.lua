--[[
	UI OFFSETS
	The size of the string should be subtracted from these values.
	Draw the text with color 0,0,0
	Subtract 1 from x and y, then draw again with color 255, 255, 255
]]
local offsets = {	
	--turn segment text
	turnCount = {x = 121, y = 9},
	scrapCount = {x = 77, y = 26},

	--card draw positions, only draw these once
	selectedUnit = {x = 148 , y = 0},
	targetUnit = {x = 728, y = 0},

	--unit card text, locations relative to the unit card as a whole
	hp = {x = 48, y = 26},
	def = {x = 103, y = 26}
}

