local Gamestate = require "lib.hump.gamestate"
class = require "lib.slither"
local cache = require "lib.cache"
--hax audio
menuMusic = cache.music("sfx/ld31d.ogg")
menuMusic:setLooping(true)
soundfx = {
	fire = cache.soundEffect("sfx/gunfire.wav"),
	laser = cache.soundEffect("sfx/railgun.wav"),
	death = cache.soundEffect("sfx/explosion.wav"),
}
soundfx.fire:setVolume(0.75)
-- FIXME
DEBUGGING = false

local function requireAll(dir)
	local items = love.filesystem.getDirectoryItems(dir)
	local prefix = dir .. "."
	local output = {}
	for _, filename in ipairs(items) do
		local basename = filename:match("^(.+)%.lua$")
		if basename then
			output[basename] = require(prefix .. basename)
		end
	end
	return output
end

function love.load(args)
	local w, h = 1280, 720
	local scaleFactor = 1

	do
		local rw, rh = love.window.getDesktopDimensions()
		local xscale, yscale = math.floor(rw/w), math.floor(rh/h)
		scaleFactor = math.min(xscale, yscale)

		if w*scaleFactor == rw and h*scaleFactor == rh and scaleFactor > 1 then
			-- We're not going fullscreen
			scaleFactor = scaleFactor - 1
		end
	end

	love.window.setMode(w*scaleFactor, h*scaleFactor)
	love.window.setTitle("PC WARS")
	love.graphics.getDefaultFilter("nearest", "nearest")

	states = requireAll("state")
	classes = requireAll("class")

	local initialState = args[2] or "splash"
	Gamestate.registerEvents()

	local origGetPosition = love.mouse.getPosition
	function love.mouse.getPosition()
		local x, y = origGetPosition()
		return math.floor(x/scaleFactor), math.floor(y/scaleFactor)
	end

	local mapping =
	{
		["l"] = "return",
		["r"] = "escape",
		["wu"] = "up",
		["wd"] = "down",
	}

	function love.mousepressed(x, y, button)
		--return Gamestate.mousepressed(math.floor(x/scaleFactor), math.floor(y/scaleFactor), button)
		return Gamestate.keypressed(mapping[button] or "escape")
	end

	function love.mousereleased(x, y, button)
		--return Gamestate.mousereleased(math.floor(x/scaleFactor), math.floor(y/scaleFactor), button)
		return Gamestate.keyreleased(mapping[button] or "escape")
	end

	function love.draw()
		love.graphics.scale(scaleFactor)
		return Gamestate.draw()
	end

	Gamestate.switch(states[initialState])
end
